#include <iostream>
#include <vector>
#include <memory>

class GraphNode
{
    public:
        // assignalbe by the system
        long index ;
        // assignable by the user to the vertext itself
        int data  ;
        long identifier ; 
        // Weight of edge from neighbor
        int weight ;

        GraphNode() { } ;
        
        GraphNode(const GraphNode & other) {
            this->index = other.index ;
            this->data = other.data ;
            this->identifier = other.identifier ;
            this->weight = other.weight ;

        } ;

        GraphNode& operator=(GraphNode other) {
            this->index = other.index ;
            this->data = other.data ;
            this->identifier = other.identifier ;
            this->weight = other.weight ;
            return *this ;
        };
};


struct GraphPathSearchResultModel {
    long numOfPaths = 0 ; 
    std::vector<std::vector<long>> paths ;
} ;

class Graph {
    private:
        std::vector<std::vector<GraphNode *>> adjancencyList ; 
        std::vector<long> currentPath ;
        GraphPathSearchResultModel result ;
        
        bool isInVector(std::vector<long> vec , long item) {
            bool exists = false ;
            for(long i = 0 ; i < vec.size() ; i++) {
                if(item == vec[i]) {
                    exists = true ;
                    break;
                }
            }
            return exists ;
        }

        
        
        GraphPathSearchResultModel* recursivePrivate(long startIdentifier , long endIdentifier , std::vector<long> currentPath , GraphPathSearchResultModel * model ) {
            currentPath.emplace_back(startIdentifier) ; 
            if(startIdentifier == endIdentifier) {
                model->numOfPaths++ ;
                model->paths.emplace_back(currentPath) ;
                return model ;
            }
            std::vector<GraphNode *> adjacents = adjancencyList[startIdentifier] ;
            adjacents.erase(adjacents.begin()) ;

            while(!adjacents.empty()) {
                GraphNode* item = adjacents.back() ;
                adjacents.pop_back() ;
                bool t2 = isInVector(currentPath , item->identifier) ;
                if(!t2) {
                    std::cout << "yep mate !!!" << std::endl ;
                    recursivePrivate(item->identifier , endIdentifier , currentPath , model) ;
                }
            }

            return model ;


        }

        long findIndexOfVertex(long identifier) {
            long startIndex = 0 ;
            for(int i = 0 ; i < adjancencyList.size() ; i++) {
                 std::vector<GraphNode *> tempList = adjancencyList[i] ;
                 GraphNode* firstElement = tempList[0] ;
                 if(firstElement->identifier == identifier) {
                    startIndex = firstElement->index ;
                    break;
                 }
            }
            return startIndex ;
        }

    public:
        
       
        
        Graph() { } ;
        ~Graph() { 
            // delete all of the elements in the adjancency list ?
        } ;

        void addVertexToGraph(int data , long identifier) { 
            // create the node on the heap memory
            GraphNode * temp  = new GraphNode() ;
            temp->data = data ; 
            temp->index = adjancencyList.size();
            temp->identifier = identifier ;
            
            // Add it to the adjancency list
            std::vector<GraphNode *> tempList ;
            tempList.emplace_back(temp) ;
            adjancencyList.emplace_back(tempList) ;

         } ;

        void addEdge(long startVertexIdentifier , long destinationVertexIdentifier , int weight) {
            // find the start vertext
            long startIndex = findIndexOfVertex(startVertexIdentifier) ;
            long destinationIndex = findIndexOfVertex(destinationVertexIdentifier) ;

            std::vector<GraphNode *> destinationTempList = adjancencyList[destinationIndex] ;
            std::vector<GraphNode *> startTempList = adjancencyList[startIndex] ;
            
            GraphNode* tempNode = destinationTempList[0] ;
            GraphNode* newTempNode = new GraphNode(*tempNode) ;
            newTempNode->weight = weight ;
            startTempList.emplace_back(newTempNode) ;
            adjancencyList[startIndex] = startTempList ;

            tempNode = startTempList[0] ;
            newTempNode = new GraphNode(*tempNode) ;
            newTempNode->weight = weight ;
            destinationTempList.emplace_back(newTempNode) ;
            adjancencyList[destinationIndex] = destinationTempList ;
            
         } ;

        void printOut() {
            for(long i = 0 ; i < adjancencyList.size() ; i++) {
                std::vector<GraphNode *> list = adjancencyList[i] ;
                std::cout << "neighbors of vertex " << list[0]->identifier << ": \n" ;
                for(long j = 1 ; j < list.size() ; j++) {
                    std::cout << " " << list[j]->identifier << " ," ;
                }
                std::cout << "\n" ;
            }
        }
        
        GraphPathSearchResultModel findPathBetweenVerteces(long startIdentifier , long endIdentifier) {
            auto t1 = recursivePrivate(startIdentifier , endIdentifier , currentPath , &result) ;  
            return *t1 ;
        }

        
         
} ;



int main () {
   std::cout << "Seems to compile" << std::endl ;

   // create the graph object 
   Graph graph1 = Graph() ;

   // create verteces and add them to the graph
   graph1.addVertexToGraph(10 , 0) ;
   graph1.addVertexToGraph(11 , 1) ;
   graph1.addVertexToGraph(12 , 2) ;
   graph1.addVertexToGraph(13 , 3) ;
   graph1.addVertexToGraph(14 , 4) ;
   graph1.addVertexToGraph(15 , 5) ;
   graph1.addVertexToGraph(16 , 6) ;

   // Add edges between verteces
   graph1.addEdge(0 , 1 , 1) ;
   graph1.addEdge(1 , 2 , 1) ;
   graph1.addEdge(1 , 3 , 1) ;
   graph1.addEdge(2 , 3 , 1) ;
   graph1.addEdge(3 , 4 , 1) ;
   graph1.addEdge(5 , 6 , 1) ;
   graph1.addEdge(4 , 6 , 1) ;
   graph1.addEdge(4 , 5 , 1) ;
   
   graph1.printOut() ;

   
   int t = graph1.recursiveTest(0 , 100) ;
   
   auto res = graph1.findPathBetweenVerteces(0 , 6) ;

   if(res.numOfPaths != 0) {
    for(int i = 0 ; i < res.paths.size() ; i++) {
        auto item = res.paths[i] ;
        for(int j = 0 ; j < item.size() ; j++) {
            std::cout << item[j] << ", " ; 

        }
        std::cout << "\n" ;
    }
   }

   std:: cout << "Ending the program" << std::endl ;  

   return 0 ;
}