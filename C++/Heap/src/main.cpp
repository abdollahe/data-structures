#include "HeapTemplate.h"


bool biggerThanEqualInt(int val1 , int val2) {
    return val1 >= val2 ;
}

int main() {
    std::cout << "yep !!" << std::endl ;

    HeapTemplate<int> heap(HeapTreeType::MIN_HEAP , biggerThanEqualInt) ;

    heap.insertNode(9) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(6) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(4) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(3) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(3) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(2) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(1) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(5) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ; 
    heap.insertNode(7) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(8) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    heap.insertNode(4) ;
    heap.printHeap() ;
    std::cout << "=====================\n" ;
    auto ex = heap.extract() ;
    heap.printHeap() ;
    
    




}