#ifndef HEAP_TEMPLATE_H
#define HEAP_TEMPLATE_H

#include <iostream>
#include <vector>
#include <functional>

enum class HeapTreeType {
    MAX_HEAP ,
    MIN_HEAP
} ;

enum class HeapOperationType {
    INSERTION , 
    EXTRACTION
} ;

template <typename T> class HeapTemplate {
     private:
        std::vector<T> heapArray ;
        HeapTreeType heapTreeType ;
        std::function<bool(T ,T)> biggerThanFunction ;
        
        void restoreOrderInsertion(long insertIndex) {
            if(insertIndex == 1)
                return ;
            bool done = false ;
            auto currentIndex = insertIndex ;

            while(!done) {
                    auto parentIndex = currentIndex / 2 ;
                    T currentValue = heapArray[currentIndex] ;
                    T parentValue = heapArray[parentIndex] ;
                    bool predicate ; 
                    if(heapTreeType == HeapTreeType::MAX_HEAP) {
                       predicate = biggerThanFunction(parentValue , currentValue) ;
                    }
                    else {
                       predicate = biggerThanFunction(currentValue , parentValue) ;
                    }
                    if(predicate) {
                        done = true ;
                    }
                    else {
                        swap(parentIndex , currentIndex , HeapOperationType::INSERTION) ;
                    }
                }
        }
        
        void swap(long &parentIndex , long &childIndex , HeapOperationType swapType) {
            T temp = heapArray[parentIndex] ;
            heapArray[parentIndex] = heapArray[childIndex] ;
            heapArray[childIndex] = temp ;
            if(swapType == HeapOperationType::EXTRACTION)
                parentIndex = childIndex ;
            else if (swapType == HeapOperationType::INSERTION)    
                childIndex = parentIndex ; 
        } ;
         
     public:
        HeapTemplate(HeapTreeType type , std::function<bool(T , T)> biggerThanFunction) {
            this->biggerThanFunction = biggerThanFunction ;
            heapTreeType = type ;
            heapArray.push_back(NULL) ;
            std::cout << "Heap tree created!" << std::endl ;

        } ;    

        ~HeapTemplate() {
            heapArray.clear() ;
            std::cout << "Heap tree destroyed!" << std::endl ;
        }

        void insertNode(T value) {
            heapArray.push_back(value) ;
            restoreOrderInsertion( static_cast<long>(heapArray.size()) - 1 ) ;
        } ;

        void printHeap() {
            for(long i = 1 ; i < heapArray.size() ; i++) {
                std::cout << heapArray[i] << " " ;
            } ;
            std::cout << std::endl ;
        }

        T extract() {
            T root = heapArray[1] ;
            T lastItem = heapArray.back() ;
            heapArray.pop_back() ;
        

            heapArray[1] = lastItem ;
            bool done = false ;
            long parentIndex = 1 ;
            
            if(heapTreeType == HeapTreeType::MAX_HEAP) {
                while(!done) {
                    long leftChildIndex = 2 * parentIndex ;
                    long rightChildIndex = 2 * parentIndex + 1 ;
                    bool isLeftChildBigger = (biggerThanFunction(heapArray[leftChildIndex] , heapArray[parentIndex])  && leftChildIndex < heapArray.size()) ;
                    bool isRightChildBigger = (biggerThanFunction(heapArray[rightChildIndex] , heapArray[parentIndex]) && rightChildIndex < heapArray.size()) ;
                    if(isLeftChildBigger && isRightChildBigger) {
                        // find max between children ;
                        bool leftChildBiggerThanRightChild = biggerThanFunction(heapArray[leftChildIndex] , heapArray[rightChildIndex]) ;
                        if(leftChildBiggerThanRightChild) 
                            swap(parentIndex , leftChildIndex, HeapOperationType::EXTRACTION) ;
                        else 
                            swap(parentIndex , rightChildIndex, HeapOperationType::EXTRACTION) ;
                    }
                    else if (isLeftChildBigger && !isRightChildBigger) 
                        swap(parentIndex , leftChildIndex, HeapOperationType::EXTRACTION) ;
                    else if (!isLeftChildBigger && isRightChildBigger) 
                        swap(parentIndex , rightChildIndex, HeapOperationType::EXTRACTION) ;
                    else 
                        done = true ;
                }
            }
            else {
                while(!done) {
                    long leftChildIndex = 2 * parentIndex ;
                    long rightChildIndex = 2 * parentIndex + 1 ;
                    bool isLeftChildSmaller = (biggerThanFunction(heapArray[parentIndex],heapArray[leftChildIndex]) && leftChildIndex < heapArray.size()) ;
                    bool isRightChildSmaller = (biggerThanFunction(heapArray[parentIndex], heapArray[rightChildIndex]) && rightChildIndex < heapArray.size()) ;
                    
                    if(isLeftChildSmaller && isRightChildSmaller) {
                        // find max between children ;
                        bool leftChildSmallerThanRightChild = biggerThanFunction(heapArray[rightChildIndex] , heapArray[leftChildIndex]) ;
                        if(leftChildSmallerThanRightChild) 
                            swap(parentIndex , leftChildIndex, HeapOperationType::EXTRACTION) ;
                        else 
                            swap(parentIndex , rightChildIndex, HeapOperationType::EXTRACTION) ;
                    }
                    else if (isLeftChildSmaller && !isRightChildSmaller) 
                        swap(parentIndex , leftChildIndex, HeapOperationType::EXTRACTION) ;
                    else if (!isLeftChildSmaller && isRightChildSmaller) 
                        swap(parentIndex , rightChildIndex, HeapOperationType::EXTRACTION) ;
                    else 
                        done = true ;
                }
            }

            

            return root ;
        }

} ;


#endif