#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include "TreeNode.h"



enum class TraversalType {
    InOrder ,
    PreOrder,
    PostOrder
} ;

template <typename T> struct NextSuccessor {
    TreeNode<T> *node ;
    bool continueSearch = true ;
    bool found = false ;

    NextSuccessor(TreeNode<T> *node) {
        this->node = node ;
    }
} ;

template <typename T> class BinarySearchTree {
    private: 
        long size  ;
        TreeNode<T>* root ;

        TreeNode<T> * createNewTreeNode(T value , long nodeKey) {
            TreeNode<T>* temp = new TreeNode(value , nodeKey) ; 
            size++ ;

            return temp ;
        }

        TreeNode<T>* insertRecursive(TreeNode<T>* rootNode , T value , long nodeKey) {
            if(rootNode == nullptr) { 
                return createNewTreeNode(value , nodeKey) ;    
            }
            else if (nodeKey > rootNode->key) {
                rootNode->rightChild = insertRecursive(rootNode->rightChild , value  , nodeKey) ;
            }
            else if (nodeKey < rootNode->key) {
                rootNode->leftChild = insertRecursive(rootNode->leftChild , value , nodeKey) ;
            }
            return rootNode ;
        } ; 

        TreeNode<T>* searchRecursive(TreeNode<T> * rootNode , long nodeKey) {
            if(rootNode->key == nodeKey)
                return rootNode ;
            else if (nodeKey > rootNode->key) {
                return searchRecursive(rootNode->rightChild , nodeKey) ; 
            }
            else if (nodeKey < rootNode->key) {
                 return searchRecursive(rootNode->leftChild , nodeKey) ;
            } 
        }

        // void printTreeRecursive(TreeNode<T> * rootNode) {
        //     if(rootNode == nullptr)
        //         return ;
        //     printTreeRecursive(rootNode->leftChild) ;
        //     std::cout << "id: " << rootNode->key << " with value: " << rootNode->value << std::endl ; 
        //     printTreeRecursive(rootNode->rightChild) ;    
        // }

        NextSuccessor<T>* findInorderSuccessorRecursive(TreeNode<T>* root , long targetNodeKey) {
            
            NextSuccessor<T>* temp = new NextSuccessor<T>(nullptr) ;
            
            if(root == nullptr) {
                return temp ;
            }
            else if (root->key == targetNodeKey) {
                if(root->rightChild == nullptr) {
                    // go back and find first parent with right child 
                    temp->node = root ;
                    temp->continueSearch = false ;
                    temp->found = false ;
                    return temp ;
                }
                else {
                    temp->node = findMostLeftLeaf(root->rightChild) ;
                    temp->found = true ; 
                    temp->continueSearch = false ;
                    return temp ;
                }
            }    
            temp = findInorderSuccessorRecursive(root->leftChild , targetNodeKey) ;
            
            // If left child was empty or target node not found in left child then 
            // search in right child 
            if(temp->continueSearch)
                temp = findInorderSuccessorRecursive(root->rightChild , targetNodeKey) ;

            else if (!temp->found) {
                temp->node =  root ;
                temp->found = true ;    
            }
                
            return temp ;
        }

        TreeNode<T>* deleteNodeRecursive(TreeNode<T>* rootNode , long targetNodeKey) {
             
            if (targetNodeKey > rootNode->key) {
                rootNode->rightChild = deleteNodeRecursive(rootNode->rightChild , targetNodeKey) ; 
            }
            else if (targetNodeKey < rootNode->key) {
                rootNode->leftChild = deleteNodeRecursive(rootNode->leftChild , targetNodeKey) ;
            } 

            else if (targetNodeKey == rootNode->key) {
                if (rootNode->leftChild == nullptr && rootNode->rightChild == nullptr) {
                    delete rootNode ;
                    return nullptr ;
                }
                else if (rootNode->leftChild == nullptr && rootNode->rightChild != nullptr) {
                    delete rootNode ;
                    return rootNode->rightChild ;
                }
                else if (rootNode->leftChild != nullptr && rootNode->rightChild == nullptr) {
                    delete rootNode ;
                    return rootNode->leftChild ;
                }
                
                auto inOrderSuccessor = findInorderSuccessorRecursive(root , rootNode->key) ;
                rootNode->key = inOrderSuccessor->node->key ;
                rootNode->value = inOrderSuccessor->node->value ;

                rootNode->rightChild = deleteNodeRecursive(rootNode->rightChild , rootNode->key) ;                
            }
            
            return rootNode ;
        } 

        void inorderTraversal(TreeNode<T>* node) {
            if(node == nullptr)
                return ;
            inorderTraversal(node->leftChild) ;
            std::cout << "id: " << node->key << " with value: " << node->value << std::endl ;    
            inorderTraversal(node->rightChild) ;
        }

        void preorderTraversal(TreeNode<T>* node) {
            if(node == nullptr)
                return ;
            std::cout << "id: " << node->key << " with value: " << node->value << std::endl ;
            preorderTraversal(node->leftChild) ;
            preorderTraversal(node->rightChild) ;
        }
        
        void postorderTraversal(TreeNode<T>* node) {
            if(node == nullptr)
                return ;
            postorderTraversal(node->leftChild) ;
            postorderTraversal(node->rightChild) ;
            std::cout << "id: " << node->key << " with value: " << node->value << std::endl ;
        }
         
    
    public:
        BinarySearchTree() {
            size = 0 ; 
            root = nullptr ;
            std::cout << "Binary tree created" << std::endl ;
        } ; 

        ~BinarySearchTree() {
            std::cout << "Binary tree distroyed" << std::endl ;
        } ;

        bool isTreeEmpty() {
            return size == 0 ;
        }

        void insertNode(T value , long nodeKey) { 
            this->root = insertRecursive(this->root , value , nodeKey) ; 
        } ; 

        TreeNode<T>* searchNode(long nodeKey) { 
            return searchRecursive(root , nodeKey) ;
        } ;

        void printTree(TraversalType traversalType) {
            switch (traversalType)
            {
            case TraversalType::InOrder:
                inorderTraversal(root) ;
                break;
            
            case TraversalType::PreOrder:
                preorderTraversal(root) ;
                break;

            case TraversalType::PostOrder:
                postorderTraversal(root) ;
                break;

            }
        }

        TreeNode<T>* findMostLeftLeaf(TreeNode<T> * node) {
            while (node != nullptr && node->leftChild != nullptr)
                node = node->leftChild;
            return node;
        }

        TreeNode<T>* findMostRightLeaf(TreeNode<T> * node) {
            while (node != nullptr && node->rightChild != nullptr)
                node = node->rightChild;
            return node;
        } 

    
        TreeNode<T>* findInOrderSuccessor(long nodeKey) {
            auto t1 = findInorderSuccessorRecursive(root , nodeKey) ;
            
            return t1->node ;
        }

        void deleteNode(long nodeKey) {
            deleteNodeRecursive(root , nodeKey) ;
        }
        
} ; 


#endif 