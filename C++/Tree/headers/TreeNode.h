
#ifndef TREE_NODE_H
#define TREE_NODE_H


#include <iostream>
#include <vector>
#include <memory>


template <typename T> class TreeNode {
    public:
        
        long key ; 
        T value ; 
        TreeNode<T>* leftChild ;
        TreeNode<T>* rightChild ;   
        
        TreeNode(T value , int key) : value(value) , key(key) { 
            leftChild = nullptr ;
            rightChild = nullptr ;
        } ;

        ~TreeNode() {
            leftChild = nullptr ;
            rightChild = nullptr ;
        }
} ;

#endif 


