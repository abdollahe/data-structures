# In the name of Allah
#============================

# cmake setup
cmake_minimum_required(VERSION 3.10)

# set project name
project(BinaryTreeTemplate)

# include the header files
include_directories(headers)

# set flags required for building
set(CMAKE_BUILD_TYPE Debug)

# gather source files manually
#set(SOURCES src/exercise_3_sailing_ship.cpp src/exercise_2_sailing_ship.cpp)

# set(PROJECT_LINK_LIBS libnode.so)
# link_directories(/usr/lib)

# gather all source files in folder
file(GLOB SOURCES "src/*.cpp")

# add executables
add_executable(binary_tree ${SOURCES})
# target_link_libraries(linked_list ${PROJECT_LINK_LIBS} )

