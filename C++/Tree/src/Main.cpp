
#include "BinaryTree.h"


int main() {


    BinarySearchTree<int> binaryTree ;

    int data1 = 20 ;
    long key1 = 20L ;
    binaryTree.insertNode(data1 ,key1) ; 
     
    data1 = 22 ;
    key1 = 22L ;
    binaryTree.insertNode(data1 ,key1) ; 
    
    data1 = 8 ;
    key1 = 8L ;
    binaryTree.insertNode(data1 ,key1) ; 
    
    data1 = 4 ;
    key1 = 4L ;
    binaryTree.insertNode(data1 ,key1) ; 
    
    data1 = 12 ;
    key1 = 12L ;
    binaryTree.insertNode(data1 ,key1) ; 
    
    data1 = 10 ;
    key1 = 10L ;
    binaryTree.insertNode(data1 ,key1) ; 
    
    data1 = 14 ;
    key1 = 14L ;
    binaryTree.insertNode(data1 ,key1) ; 
     
    binaryTree.printTree(TraversalType::InOrder) ;

    binaryTree.deleteNode(8L) ;

    binaryTree.printTree(TraversalType::InOrder) ;
     
     
    return 0 ;
}