#ifndef EDGE_NODE_H
#define EDGE_NODE_H

#include <iostream>


template <typename VertextDataType, typename WeightType> class EdgeNode {
    public:
        VertextDataType destinationVertex ;
        WeightType weight ;
        u_int64_t index ;
        EdgeNode * nextVertex ;

        EdgeNode(VertextDataType destination , WeightType weight , u_int64_t index) {
            this->destinationVertex = destination ;
            this->weight = weight ;
            this->index = index ;
            this->nextVertex = nullptr ;
        }
        ~EdgeNode() {
            if(nextVertex != nullptr)
                delete nextVertex ;
        }
} ;


template <typename VertextDataType, typename WeightType> class EdgeNodeSimple {
    public:
        VertextDataType destinationVertex ;
        WeightType weight ;
        u_int64_t index ;

        EdgeNodeSimple(VertextDataType destination , WeightType weight , u_int64_t index) {
            this->destinationVertex = destination ;
            this->weight = weight ;
            this-> index = index ;
        }
} ;



#endif

