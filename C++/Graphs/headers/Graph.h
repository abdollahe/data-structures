#ifndef GRAPH_H
#define GRAPH_H

#include "Edge_Node.h"
#include <vector>
#include <memory>
#include <algorithm>
#include <queue>
#include <set>
#include <iterator>

template <typename VertexDataType , typename WeightType> class Graph {
    private:
        std::vector<EdgeNode<VertexDataType,WeightType> *> adjacencyList ;
        std::set<VertexDataType> indexSet ;
        bool isDirected ;
        bool isWeighted ;
        u_int64_t numOfVertexes ;
        WeightType defaultWeightValue ;

     public:    
        long findVertexIndex(VertexDataType targetVertex) {     
            auto t0 = adjacencyList.size() ;

            long index = -1L ;
            
            for(long i = 0 ; i < adjacencyList.size() ; i++) {
                if(adjacencyList[i]->destinationVertex == targetVertex) {
                    index = i ;
                }
            }
            return index ;
        } ;

        void parseToEndAndAddEdge(long startIndex, VertexDataType destinationVertex , WeightType edgeWeight) {
            auto t1 = adjacencyList[startIndex] ;

            while(t1->nextVertex != nullptr) {
                t1 = t1->nextVertex ;                
            }

            t1->nextVertex = new EdgeNode(destinationVertex , edgeWeight , numOfVertexes) ;

            // auto t2 = findVertexIndex(destinationVertex) ;
            // if(t2 != -1) {

            // }
            // else {
                
            // }
            numOfVertexes++ ;
        }
        
   
        Graph(WeightType defaultWeightValue) {
            this->defaultWeightValue = defaultWeightValue ;
        } ;
        ~Graph() {
            while(!adjacencyList.empty()) {
                adjacencyList.pop_back() ;
            }
        } ;

        void addVertex(VertexDataType destinationVertex , WeightType weight) {
            adjacencyList.emplace_back(new EdgeNode<VertexDataType , WeightType>(destinationVertex , weight , numOfVertexes)) ;
            indexSet.insert(adjacencyList.size()) ; 
            numOfVertexes++ ;
        };

        void addEdgeToVertex(VertexDataType startVertex, VertexDataType destinationVertex , WeightType edgeWeight) {
            auto startVertexTargetIndex = findVertexIndex(startVertex) ;
            auto destinationVertexIndex = findVertexIndex(destinationVertex) ;
            
            if(startVertexTargetIndex != -1) {
                parseToEndAndAddEdge(startVertexTargetIndex , destinationVertex , edgeWeight) ;
            }
            else {
                addEdgeToVertex(startVertex) ;
                addEdgeToVertex(startVertex , destinationVertex , edgeWeight) ;
            }
             
            if(destinationVertexIndex != -1) {
                parseToEndAndAddEdge(destinationVertexIndex , startVertex , edgeWeight) ;
            }
            else {
                addEdgeToVertex(destinationVertex) ;
                addEdgeToVertex(destinationVertex , startVertex , edgeWeight) ;
            } 
        } ;

    
        void addEdgeToVertex(VertexDataType startVertex) {    
            adjacencyList.emplace_back(new EdgeNode(startVertex , defaultWeightValue , numOfVertexes)) ;
            indexSet.insert(startVertex) ; 
            numOfVertexes++ ;
        } ;
        
        void BreadthFirstSearchPrint(VertexDataType startVertex) {
            std::queue<EdgeNode<VertexDataType,WeightType> *> q ;
            std::set<VertexDataType> addedSet ;
            std::set<VertexDataType> unvisitedElements ;

            unvisitedElements = indexSet ;
            
            auto sVertex = startVertex ;

            while(!unvisitedElements.empty()) {
                if(unvisitedElements.size() != adjacencyList.size()) {
                    sVertex = *(unvisitedElements.begin()) ;
                    unvisitedElements.erase(sVertex) ;
                }

                auto index = findVertexIndex(sVertex) ;

                auto arrayElement = adjacencyList[index] ;

                q.push(arrayElement) ;
                addedSet.insert(arrayElement->destinationVertex) ;

                while(!q.empty()) {
                    // retrieve first element from queue
                    auto size1 = q.size() ;
                    auto front = q.front() ;
                    q.pop() ;
                    unvisitedElements.erase(front->destinationVertex) ;
                    auto size2 = q.size() ;

                    // print the current element
                    std::cout << front->destinationVertex << " ," ;
                
                    // add adjacent verteces to the queue.
                    EdgeNode<VertexDataType,WeightType> *temp = front ;
                    while(temp->nextVertex != nullptr) {
                        temp = temp->nextVertex ;
                        auto res = addedSet.find(temp->destinationVertex) ;
                        if(res == addedSet.end()) {
                            auto index1 = findVertexIndex(temp->destinationVertex) ;
                            if(index1 != -1) {
                                auto t1 = adjacencyList[index1] ;
                                q.push(t1) ;
                                addedSet.insert(t1->destinationVertex) ;
                            }
                        }
                    }    
                }
            }
        }
}; 

#endif