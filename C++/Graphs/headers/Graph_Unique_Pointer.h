#ifndef GRAPH_UNIQUE_POINTER_H
#define GRAPH_UNIQUE_POINTER_H

#include "Edge_Node.h"
#include <vector>
#include <memory>
#include <algorithm>
#include <queue>
#include <set>
#include <iterator>

template <typename VertexDataType , typename WeightType> class Graph {
    private:
        std::vector<std::unique_ptr<EdgeNode<VertexDataType,WeightType>>> adjacencyList ;
        bool isDirected ;
        bool isWeighted ;
        u_int64_t numOfVertexes ;
        WeightType defaultWeightValue ;

     public:    
        long findVertexIndex(VertexDataType targetVertex) {     
            auto t0 = adjacencyList.size() ;

            long index = -1L ;
            
            for(long i = 0 ; i < adjacencyList.size() ; i++) {
                if(adjacencyList[i].get()->destinationVertex == targetVertex) {
                    index = i ;
                }
            }
            return index ;
        } ;

        void parseToEndAndAddEdge(long startIndex, VertexDataType destinationVertex , WeightType edgeWeight) {
            auto t1 = std::move(adjacencyList[startIndex]) ;
            auto t2 = t1.get() ;

            while(t2->nextVertex != nullptr) {
                t2 = t2->nextVertex ;                
            }

            t2->nextVertex = new EdgeNode(destinationVertex , edgeWeight , numOfVertexes) ;
            adjacencyList[startIndex] = std::move(t1) ;

        }
        
        Graph(WeightType defaultWeightValue) {
            this->defaultWeightValue = defaultWeightValue ;
        } ;
        
        ~Graph() {

        } ;

        void addVertex(VertexDataType destinationVertex , WeightType weight) {
            adjacencyList.emplace_back(std::make_unique<EdgeNode>(destinationVertex , weight , numOfVertexes)) ;
            numOfVertexes++ ;
        };

        void addVertex(VertexDataType destinationVertex) {
            EdgeNode<VertexDataType,WeightType> *temp = new EdgeNode(destinationVertex , defaultWeightValue , numOfVertexes) ;
            adjacencyList.emplace_back(std::move(std::unique_ptr<EdgeNode<VertexDataType,WeightType>>(temp))) ;
            numOfVertexes++ ;
        }; 

        void addEdgeToVertex(VertexDataType startVertex, VertexDataType destinationVertex , WeightType edgeWeight) {
            auto targetIndex = findVertexIndex(startVertex) ;
            parseToEndAndAddEdge(targetIndex , destinationVertex , edgeWeight) ;
        } ;
        
        void BreadthFirstSearchPrint(VertexDataType startVertex) {
            std::queue<EdgeNode<VertexDataType,WeightType>> q ;
            std::set<VertexDataType> visitedSet ;

            auto index = findVertexIndex(startVertex) ;

            auto arrayElement = adjacencyList[index] ;

            q.emplace(arrayElement) ;

            while(!q.empty()) {
                // retrieve first element from queue
                EdgeNode<VertexDataType,WeightType> front = q.front() ;
                q.pop()

                // print the current element
                std::cout << front->
            }
        }
}; 

#endif