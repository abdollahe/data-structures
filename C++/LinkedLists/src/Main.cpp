#include "Linked_List.h"
#include "Node.h"

int main() {
    
     // build a linked list
     Linked_List<int> list1(LinkedListType::Doubly)  ; 
     Node<int> node(1) ; 
     list1.insertNodeEnd(std::move(node)) ;


     node.data = 2 ;

     list1.insertNodeEnd(std::move(node)) ;
     

     node.data = 3 ;

     list1.insertNodeEnd(std::move(node)) ;

     list1.printAllNodes() ;
     
     std::cout << "=================================\n" ;
     
     node.data = 4 ;

     list1.insertNodeAt(std::move(node) , 2) ;
     
     list1.printAllNodes() ;
      
     std::cout << "=================================\n" ; 

     node.data = 200 ;

     list1.insertNodeAt(std::move(node) , 0) ;
     
     list1.printAllNodes() ;

     std::cout << "=================================\n" ;

     node.data = 300 ;
     
     auto t1 = list1.getNodeCount() ;

     list1.insertNodeAt(std::move(node) , t1) ;
     
     list1.printAllNodes() ;

     std::cout << "=================================\n" ;

     auto r1 = list1.getNodeEnd() ;
     std::cout << "end value: " << r1->data << std::endl ; 
     auto r2 = list1.getNodeAt(0) ; 
     std::cout << " start value: " << r2->data << std::endl ; 
     int index = 3 ;
     auto r3 = list1.getNodeAt(index) ;
     std::cout << "index " << index  << " value: " << r3->data << std::endl ; 
     
     std::cout << "=================================\n" ;

     list1.deleteNodeEnd() ;

     list1.printAllNodes() ; 

     std::cout << "=================================\n" ;

     list1.deleteNodeAt(0) ;

     list1.printAllNodes() ;

     std::cout << "=================================\n" ;

     list1.deleteNodeAt(index) ;

     list1.printAllNodes() ;

     std::cout << "=================================\n" ;


     std::cout << "\n" ;


    return 0 ; 
}