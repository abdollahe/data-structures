#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <Node.h>

enum class LinkedListType {
    Singly, 
    Doubly
} ;

template <typename T> class Linked_List {
   private:
    long node_count ;
    Node<T>* head_node ;
    Node<T>* start_node ;
    LinkedListType linkedListType ; 

    Node<T>* traverseTo(long index) {
        Node<T> *tempNode ; 
        try {
            long currentNodeIndex = 0 ;
            tempNode = this->start_node ;
            while(currentNodeIndex < index) {
                tempNode = tempNode->nextNode ;
                currentNodeIndex++ ;
            }
        }

        catch (const std::exception& e) {
            std::cout << "error in traversing linked list: " << e.what() << std::endl ;      
        }
        
        return tempNode ;
    }
     
   public:

    Linked_List(LinkedListType type) {
        linkedListType = type ;
        this->head_node = nullptr ;
        this->start_node = nullptr ;
        this->node_count = 0L; 
        std::cout << "Linkedlist created\n" ;
    }  ;

    ~Linked_List() {
        // clean the memory from memory leaks
        while(!isListEmpty()) {
            deleteNodeEnd() ;
        }
        std::cout<< "LinkedList destroyed\n" ;
    } ;

    bool isListEmpty() {
        return node_count == 0 ;
    }

    long getNodeCount() {
        return this->node_count ;
    } ;
    
    void insertNodeEnd(Node<T> &&node) {
        try{
            Node<T> * tempNode = new Node<T>(node.data) ;
            tempNode->nextNode = nullptr ;

            if(this->isListEmpty()) {
                this->start_node = tempNode ;
                if(linkedListType == LinkedListType::Doubly) {
                    tempNode->previousNode = head_node ;
                }
                this->head_node = tempNode ;
                this->node_count++ ;
            }
            else {
                this->head_node->nextNode = tempNode ;
                if(linkedListType == LinkedListType::Doubly) {
                    tempNode->previousNode = head_node ;
                }
                this->head_node = tempNode ; 
                this->node_count++ ;
            }
        }
        catch(const std::exception& e) {
            std::cout << "error in inserting node: " << e.what() << std::endl ; 

        }
    }
    
    void insertNodeAt(Node<T> &&node , long position) {
        
        Node<T>* newNode = new Node(node.data) ;
        
        try {
            if(position == 0) {
            newNode->nextNode = this->start_node ;
            this->start_node = newNode ;
        }
        else {
            Node<T>* previousNode = traverseTo(position - 1) ;
            newNode->nextNode = previousNode->nextNode ; 
            if(linkedListType == LinkedListType::Doubly) {
                newNode->previousNode = previousNode ;   
            }
            previousNode->nextNode = newNode ; 
        }
        this->node_count++ ;

        }
        catch(const std::exception& e) {
            std::cout << "error in inserting node: " << e.what() << std::endl ; 
        }

    } ; 
    
    void deleteNodeEnd() {
        try {
            Node<T>* previousNode = traverseTo(this->node_count - 1) ; 
            Node<T>* currentNode  = previousNode->nextNode ;
            this->head_node = previousNode ; 
            head_node->nextNode = nullptr ;
            if(linkedListType == LinkedListType::Doubly) {
                head_node->previousNode = previousNode ;
            }
            delete currentNode ;
            node_count-- ;
        }
        catch (const std::exception& e) {
            std::cout << "error in deleting node: " << e.what() << std::endl ; 
        }
    } ;

    void deleteNodeAt(long position) {
        try {
            Node<T>* currentNode ;
            if(position == 0) {
                currentNode = start_node ;
                start_node = start_node->nextNode ;
                if(linkedListType == LinkedListType::Doubly) {
                    start_node->previousNode = nullptr ;
                }
            }
            else {
                Node<T>* previousNode = traverseTo(position - 1) ;
                currentNode = previousNode->nextNode ;
                Node<T>* nextNode  = currentNode->nextNode ;
                previousNode->nextNode = nextNode ;
                if(linkedListType == LinkedListType::Doubly) {
                    nextNode->previousNode = previousNode ;
                }
            }
            delete currentNode ;
            this->node_count-- ;
        }
        catch( const std::exception& e) {
            std::cout << "error in deleting node: " << e.what() << std::endl ; 
        }
        
    } ;

    Node<T>* getNodeEnd() { 
        return traverseTo(this->node_count - 1) ;
    } ;
    
    Node<T>* getNodeAt(long position) {
          return traverseTo(position) ;
    } ; 
    void printNodesFromStartToEnd(long startIndex , long endIndex) {
         auto startNode = traverseTo(startIndex - 1) ;
         auto endNode = traverseTo(endIndex - 1) ;
         long tempIndex = 0 ;

         try {
            do{
                std::cout << "Node index " << ((startIndex) + tempIndex) << " at memory " << startNode << " with value: " << startNode->data << std::endl ;
                startNode = startNode->nextNode ;
                tempIndex++ ; 
            }
            while(startNode != endNode->nextNode) ;
         }
         catch (const std::exception& e) {
            std::cout << "error in printing linked list: " << e.what() << std::endl ; 
         }
    }

    void printAllNodes() {
        printNodesFromStartToEnd(1 , node_count) ;
    }
}; 

#endif