
#include <iostream>

template <typename T> class Node {
    public:
        // Data of the node
        T data ;

        // Pointer to the next node
        Node *nextNode ;
       
        // Pointer to the previous node
        Node *previousNode ;
         
        Node(T data) : data(data) { } ;

        ~Node() {
            this->nextNode = nullptr ;
            this->previousNode = nullptr ;
        }
} ;


